package life.maxima;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExamples {

    public static void main(String[] args) {
        System.out.println("Begin");
        Stream.of(5,4,3,2,1)
                .map(i -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return i;
                })
                .filter(i -> i % 2 == 0)
                .sorted()
                .forEach(System.out::println);

        System.out.println("End");
    }
}
