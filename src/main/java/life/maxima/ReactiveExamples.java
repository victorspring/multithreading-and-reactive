package life.maxima;

import reactor.core.publisher.Flux;

import java.time.Duration;

public class ReactiveExamples {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Begin");
        Flux.just(7,6,5,4,3,2,1)
                .delayElements(Duration.ofSeconds(1))
                .filter(i -> i % 2 == 0)
//                .sort()
                .subscribe(System.out::println);

        System.out.println("End");
        Thread.sleep(10000L);
    }
}
