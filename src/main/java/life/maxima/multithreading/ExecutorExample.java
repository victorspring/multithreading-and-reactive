package life.maxima.multithreading;

import java.nio.file.Files;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorExample {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool =  Executors.newFixedThreadPool(1);

        Future<Integer> future = pool.submit(() -> {
            int count = 0;
            for (int i = 1; i <= 100; i++) {
                count += i;
            }

            Thread.sleep(1000L);
            return count;
        });


        System.out.println("Waiting for result...");
        System.out.println(future.get());

        pool.shutdown();
    }
}
