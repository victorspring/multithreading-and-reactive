package life.maxima.multithreading;

public class Multithreading {

    static int n = 1;

    public static void main(String[] args) throws InterruptedException {

        Object monitor = new Object();

        Thread t0 = new Thread(() -> {
            for (int i = 0; i < 100_000_000; i++) {
                synchronized (monitor) {
                    n++;
                }

            }
        });

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 100_000_000; i++) {
                synchronized (monitor) {
                    n--;
                }
            }
        });

        t0.start();
        t1.start();

        t0.join();
        t1.join();

        System.out.println(n);
    }
}
