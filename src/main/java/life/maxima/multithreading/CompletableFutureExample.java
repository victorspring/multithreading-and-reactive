package life.maxima.multithreading;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CompletableFutureExample {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<String> future = new CompletableFuture<>();
        asyncReadFile(future);
        while (!future.isDone()){
            Thread.sleep(1);
            System.out.println("Waiting for result...");
        }

        System.out.println(future.get());

    }

    private static void asyncReadFile(CompletableFuture<String> future){
        new Thread(() -> {
            try {
                future.complete(Files.readString(Paths.get("pom.xml")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
